import math
import operator
import numpy as np
from collections import defaultdict
import re



def training(training_set):
    """
    This is the place you implement the training stage of Naive Bayes classifier. 

    The input parameters is training_set: a dictionary contains all the training documents. 
        The dict is indexed by the document id. Each document is represented as a dictionary 
        with two keys, i.e., category and content. The category is the known category
        of the document, while the words of the document is stored in content. 
        Every word of the content have been changed to lower case. The punctuations 
        have been removed from the content. 

        For example, you could get the detailed information of document 101551 (one of 
        the document in Car category under the training set) as following:
        The content:
        content = training_set["101551"]["content"]
        and the category:
        category = training_set["101551"]["category"]

        Both the content and the category of one particular document are strings. 

    You need to return two variables in this function:
    P_C: The probability of each category shown in the training set. The P_C should be 
        a dictionary. The keys are the name of the categories, and the values are the 
        probabilities of the corresponding category. For example:
        P_C["Computer_hardware"]=0.5
    P_w_C: The probabilities of each word shown in each category in the training set. 
        The P_w_C should be a dictionary of dictionaries. The first key should be 
        the word, while the second key should be category, and the value should be the 
        probability of the word shown in the category. For example:
        P_w_C["the"]["Computer_hardware"]=0.008

    These two parameters have been declared for you. Please do not remove these lines. 

    """

    P_C={}              # Please do not remove this line
    P_w_C={}            # Please do not remove this line

    # Begin your implementation after this line
    ###############################################
    
    P_C = defaultdict(float)       # P_C["Computer_hardware"]=0.5
    P_w_C = defaultdict(dict)      # P_w_C["the"]["Computer_hardware"]=0.008
    
    tot_words = 0
    dict_of_classwords = defaultdict(list) # dictionary of words belonging to each category
    
    all_words = []
    for document in training_set.keys():
        
        category = training_set[document]['category']
        P_C[category] += 1
        
        content = training_set[document]['content']
        clean_doc = re.sub(r'[^A-Za-z0-9]', ' ', content)
        doc  =  re.sub(r'[\s]', ',', clean_doc)
        
        wors = doc.split(",")
        words = list(filter(None, wors)) # fastest
        
        all_words += words
        dict_of_classwords[category] += list(words)
    
    all_unique_words = len(np.unique(np.array(all_words)))
    w = open("words_count.txt", 'w')
    
    for cat in dict_of_classwords.keys():
        
        words = dict_of_classwords[cat]
        total_words = len(words)
        
        words_array = np.array(list(words))
        unique,counts = np.unique(words_array, return_counts = True)
        unique_words = len(unique)
        dict_of_words = dict(zip(unique, counts))
        w.write(str(cat)+" "+str(total_words)+" "+str(unique_words)+"\n")
        
        for word in dict_of_words.keys():
            P_w_C[word][cat] = math.log((dict_of_words[word]+1)/float(total_words+all_unique_words))
           
    w.close()
    
    total_documents = float(len(training_set.keys()))
    for cat in P_C.keys():
        
        P_C[cat] = P_C[cat]/total_documents
  
        
        
    ################################################	
    # Finish your implementation before this line

    return P_C, P_w_C			# Please do not remove this line


def infer(document, P_C, P_w_C):
    """
    This is the place where you implement the inferring stage of Naive Bayes classification. 

    There are three input parameters, which are:
    document: A string that contains only the content of the document need to be classified. 
        The content has been converted to lower case, and the punctuations have been removed
        before feed into this function. 

    P_C: The same parameter you generated in the training stage. i.e., it contains the 
        probability of each category shown in the training set. The P_C is 	a dictionary. 
        The keys are the name of the categories, and the values are the probabilities
        of the corresponding category. For example:
        P_C["Computer_hardware"]=0.5

    P_w_C: The same parameter you generated in the training stage. 
        The probabilities of each word shown in each category in the training set. 
        The P_w_C is a dictionary of dictionaries. The first key should be 
        the word, while the second key should be category, and the value should be the 
        probability of the word shown in the category. For example:
        P_w_C["the"]["Computer_hardware"]=0.008

        The return value of this function should be the inferred category of the document. The 
        return value should be string. The return variable has been declared for you. Please do 
        not remove it.
        """

    inferred_category = "" 	# Please do not remove this line

    # Begin your implementation after this line
    ###############################################
   
    classes = list(P_C.keys())
    probs = [0.0]*len(classes)
    labels = [""]*len(classes)
    i = 0

    clean_doc = re.sub(r'[^A-Za-z0-9]', ' ', document)
    doc  =  re.sub(r'[\s]', ',', clean_doc)
    words = doc.split(",")
    words = list(filter(None, words)) # fastest
    f = open("words_count.txt", 'r')
    
    all_unique_words = 0
    
    classes_words_info = defaultdict(int)
    for line in f:
        data = line.split(" ")
        all_unique_words += int(data[2])
        
        classes_words_info[data[0]] = int(data[1])
    
    f.close()
    i = 0
    
    for class_ in classes:
        prob=0.0
        class_total_words = int(classes_words_info[class_])
        pseudo_value = math.log(1/float(class_total_words+all_unique_words))
        
        for w in words:
            value = pseudo_value
            if w in P_w_C.keys():
                if class_ in P_w_C[w].keys():
                    value = P_w_C[w][class_]
            prob = prob + value 
        
        probs[i] = (prob + math.log(P_C[class_]))
        labels[i] = class_
        i += 1

    index = np.argmax(probs)
    inferred_category = labels[index]
    #print "Predicted Class:",inferred_category


    ################################################	
    # Finish your implementation before this line

    return inferred_category			# Please do not remove this line

