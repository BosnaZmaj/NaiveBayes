 P_C = defaultdict(float)       # P_C["Computer_hardware"]=0.5
    P_w_C = defaultdict(dict)      # P_w_C["the"]["Computer_hardware"]=0.008
    
    tot_words = 0
    dict_of_classwords = defaultdict(list) # dictionary of words belonging to each category
    
    all_words = []
    for document in training_set.keys():
        
        category = training_set[document]['category']
        P_C[category] += 1
        
        content = training_set[document]['content']
        clean_doc = re.sub(r'[^A-Za-z0-9]', ' ', content)
        doc  =  re.sub(r'[\s]', ',', clean_doc)
        
        wors = doc.split(",")
        words = list(filter(None, wors)) # fastest
        
        all_words += words
        dict_of_classwords[category] += list(words)
    
    all_unique_words = len(np.unique(np.array(all_words)))
    w = open("words_count.txt", 'w')
    
    for cat in dict_of_classwords.keys():
        
        words = dict_of_classwords[cat]
        total_words = len(words)
        
        words_array = np.array(list(words))
        unique,counts = np.unique(words_array, return_counts = True)
        unique_words = len(unique)
        dict_of_words = dict(zip(unique, counts))
        w.write(str(cat)+" "+str(total_words)+" "+str(unique_words)+"\n")
        
        for word in dict_of_words.keys():
            P_w_C[word][cat] = math.log((dict_of_words[word]+1)/float(total_words+all_unique_words))
           
    w.close()
    
    total_documents = float(len(training_set.keys()))
    for cat in P_C.keys():
        
        P_C[cat] = P_C[cat]/total_documents
      
